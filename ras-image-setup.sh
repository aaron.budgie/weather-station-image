
#!/bin/bash

cd /home/pi/

# Install requirements
apt update && apt install -yq \
    python3-smbus i2c-tools git python3-dev python3-pip \
    libfreetype6-dev libjpeg-dev build-essential libsdl-dev \
    libportmidi-dev libsdl-ttf2.0-dev libsdl-mixer1.2-dev libsdl-image1.2-dev libopenjp2-7 \
    python3-RPi.GPIO hostapd fswebcam
rm -rf /var/lib/apt/lists/*

if [ ! -d /home/pi/weather-station ]
then
    git clone https://gitlab.com/aaron.budgie/weather-station.git
else
    cd weather-station
    git fetch origin
    git reset --hard origin/master
fi

pip3 install -r /home/pi/weather-station/requirements.txt
cp -f /home/pi/weather-station-image/config.txt /boot/
cp -f /home/pi/weather-station-image/modules /etc/
mv /home/pi/weather-station/dicts/credentials.json.sample /home/pi/weather-station/dicts/credentials.json

git clone https://github.com/lthiery/SPI-Py.git
cd SPI-Py
python3 setup.py install
cd ..
rm -R SPI-Py

apt clean
